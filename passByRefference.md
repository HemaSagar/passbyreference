## Pass by Reference in JavaScript

In JavaScript pass-by-reference means we pass the address of the original object(reference) to the function, rather than creating a new memory for the parameter.
Objects and arrays are passed by reference, whereas all primitive types like strings, numbers, boolean, symbols, undefined and null are passed by value in JavaScript.

**Assigning an object to a new variable**

In the code below we will see what happens to the original object when we try to change the contents of a copy object(not really).

```js
const obj1 = {'name':'Alice', 'age':20};
console.log(obj1); //{ name: 'Alice', age: 20 }

let newObj = obj1;
newObj[location] = "Bangalore";

console.log(newObj) //{ name: 'Alice', age: 20, location: 'Bangalore' }
console.log(obj1) //{ name: 'Alice', age: 20, location: 'Bangalore' }
```

Here we can see that changing the contents of **newObj** has also affected the **obj1**.

#### Why this behavior

- When we try to assign an object to another variable, we are not allocating new memory to the new variable and storing the contents in it. 
- Rather what's happening is, we are actually assigning a memory reference of the original object to the new variable.
- So both the original and new variable is now referring to the same memory location(sharing the same memory).


  

> **_NOTE:_** Changing the value of a referenced variable doesn't affect the original object. But changing the property of an object referenced by a variable will affect the original object.

**What does the above note actually mean**

Let's understand it by passing an object to a function by reference.

```js

const original = {'location': 'Bangalore', 'temperature': '27C'};

function change(referrenced){
    console.log(referrenced); //{ location: 'Bangalore', temperature: '27C' }

    //changing the properties of object
    referrenced['traffic'] = "heavy";
    referrenced['weather'] = "cloudy";

    //Changing the value of object
    referrenced = {'location': 'Hyderabad'};

    console.log(referrenced); //{ location: 'Hyderabad' };

}

change(original);
console.log(original); //{location: 'Bangalore',temperature: '27C',traffic: 'heavy',weather: 'cloudy'}

```

In the above example, we called the change function with the original object.

- When the referenced variable inside the change function tries to add new properties, they were actually reflected in the original object.
- But when the referenced variable is assigned with a new object the original object is not changed.
- This is because, as soon as the referenced variable is assigned to the new object, it lost its reference to the original object and created its own memory reference and now points to the current object.
- Therefore changes made to referenced variable further on will not affect the original object.

#### Let's observe what happens with arrays

In JavaScript, Arrays are also passed by reference. 

```js

const arr = [1,2,['hi','hello'],[]];

function changeArr(newArr){

    let x = newArr[1];//x contains 1 
    let y = newArr[2];//y contains ['hi', 'hello']
    let z = newArr[3];//z contains []

    newArr[1] = 10; // this will not change x value
    newArr[2] = ['new array']; //this will not change y value because , it is being assigned to a new array.
    newArr[3].push('new');// it will change z value because it is making changes to existing value.

    console.log(x,y,z);// 2 ,[ 'hi', 'hello' ] ,[ 'new' ]
}

changeArr(arr);
console.log(arr); // [ 1, 10, ['new array'], [ 'new' ] ]
```
In the above example, we can observe how pass-by-reference and pass-by-value affect the array contents.

- `x` is assigned with a primitive type so it is passed by value and future changes to `newArr` will not affect its value.
- `y` is assigned with an array, so it is passed by reference. Any future changes to newArr can affect `y`. But here it is observed that `y` has not changed its value.
- The reason why `y` hasn't changed is that we are assigning a new array to newArr[2]. Therefore now `y` is no longer referenced to newArr[2].
- `z` has changed its value because we are modifying the properties of the same array instead of changing the value.
- Outside the function, the original `arr` has the same values as `newArr`, because `arr` is passed by reference, not value.

#### Additional information

As we came to know that objects and arrays are pass-by-reference in JS. If you need to pass the objects and arrays by values use `JSON.parse(JSON.stringify(object))`.
This will deep copy the object and there will be no reference link between the original and copied objects.

###### Example

```js
const obj1 = {'name':'Bob'};

function modify(deepCopied){
    deepCopied.name = 'cat';
    console.log(deepCopied)// { name: 'cat' }
}

modify(JSON.parse(JSON.stringify(obj1)));
console.log(obj1)// { name: 'Bob' }
```
Here you can observe that changing the deepCopied properties doesn't affect the original obj1. This is because we are passing the deepCopy rather than the reference of the original object.

###### Resources

[Reference](https://www.scaler.com/topics/javascript/pass-by-value-and-pass-by-reference/)
[Refer to](https://stackoverflow.com/questions/6605640/javascript-by-reference-vs-by-value)
[Deep copy](https://developer.mozilla.org/en-US/docs/Glossary/Deep_copy)